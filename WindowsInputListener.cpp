#include "WindowsInputListener.h"
#include "WindowsInputMessage.h"
#include "Common/Macros.h"

CWindowsInputListener::CWindowsInputListener( CInput* pInput, CWindow& rWindow, f32 fClickSpeed, u32 uClickDistance )
	: CInputListener( pInput )
	, m_rWindow( rWindow )
	, m_eLastMouseAction( CMouseMessage::NONE )
	, m_fClickSpeed( fClickSpeed )
	, m_uClickDistanceSquared( uClickDistance * uClickDistance )
{
	CMessenger::GlobalListen( *this, CWindowsInputMessage::GetWindowsInputMessageType() );
}

CWindowsInputListener::~CWindowsInputListener()
{
	CMessenger::GlobalStopListening( *this, CWindowsInputMessage::GetWindowsInputMessageType() );
}

void CWindowsInputListener::Push( CMessage& rMessage )
{
	assert( rMessage.GetType() == CWindowsInputMessage::GetWindowsInputMessageType() );
	CWindowsInputMessage& rWindowsMessage( static_cast< CWindowsInputMessage& >( rMessage ) );
	if( rWindowsMessage.GetWindow() != &m_rWindow )
	{
		return;
	}
	switch( rWindowsMessage.GetWindowsInputMessage() )
	{
		case WM_KEYDOWN:
		{
			SetButton( static_cast< u32 >( rWindowsMessage.GetParameter1() ), true );
			break;
		}
		case WM_KEYUP:
		{
			SetButton( static_cast< u32 >( rWindowsMessage.GetParameter1() ), false );
			break;
		}
		case WM_DESTROY:
		{
			SetButton( 0, true );
			break;
		}
		case WM_SIZE:
		{
			m_rWindow.Resize( static_cast< f32 >( LOWORD( rWindowsMessage.GetParameter2() ) ), 
				static_cast< f32 >( HIWORD( rWindowsMessage.GetParameter2() ) ) );
			break;
		}
		case WM_LBUTTONDBLCLK:
		{
			u32 uX( ( static_cast< u32 >( rWindowsMessage.GetParameter2() ) << 16 ) >> 16 );
			u32 uY( static_cast< u32 >( rWindowsMessage.GetParameter2() ) >> 16 );
			CU32Vector2 oOriginalPos( CU32Vector2( uX, uY ) );
			CF32Vector2 oPos( ConvertCoordinates( oOriginalPos ) );
			CMouseMessage oMouseMessage( CMouseMessage::LEFT_BUTTON_DOUBLE_CLICK, oPos.X(), oPos.Y() );
			CMessenger::GlobalPush( oMouseMessage ); 
			CheckForCombo( CMouseMessage::LEFT_BUTTON_DOUBLE_CLICK, oOriginalPos );
			break;
		}
		case WM_RBUTTONDBLCLK:
		{
			u32 uX( ( static_cast< u32 >( rWindowsMessage.GetParameter2() ) << 16 ) >> 16 );
			u32 uY( static_cast< u32 >( rWindowsMessage.GetParameter2() ) >> 16 );
			CU32Vector2 oOriginalPos( CU32Vector2( uX, uY ) );
			CF32Vector2 oPos( ConvertCoordinates( oOriginalPos ) );
			CMouseMessage oMouseMessage( CMouseMessage::RIGHT_BUTTON_DOUBLE_CLICK, oPos.X(), oPos.Y() );
			CMessenger::GlobalPush( oMouseMessage ); 
			CheckForCombo( CMouseMessage::RIGHT_BUTTON_DOUBLE_CLICK, oOriginalPos );
			break;
		}
		case WM_LBUTTONDOWN:
		{
			u32 uX( ( static_cast< u32 >( rWindowsMessage.GetParameter2() ) << 16 ) >> 16 );
			u32 uY( static_cast< u32 >( rWindowsMessage.GetParameter2() ) >> 16 );
			CU32Vector2 oOriginalPos( CU32Vector2( uX, uY ) );
			CF32Vector2 oPos( ConvertCoordinates( oOriginalPos ) );
			CMouseMessage oMouseMessage( CMouseMessage::LEFT_BUTTON_DOWN, oPos.X(), oPos.Y() );
			CMessenger::GlobalPush( oMouseMessage ); 
			CheckForCombo( CMouseMessage::LEFT_BUTTON_DOWN, oOriginalPos );
			break;
		}
		case WM_RBUTTONDOWN:
		{
			u32 uX( ( static_cast< u32 >( rWindowsMessage.GetParameter2() ) << 16 ) >> 16 );
			u32 uY( static_cast< u32 >( rWindowsMessage.GetParameter2() ) >> 16 );
			CU32Vector2 oOriginalPos( CU32Vector2( uX, uY ) );
			CF32Vector2 oPos( ConvertCoordinates( oOriginalPos ) );
			CMouseMessage oMouseMessage( CMouseMessage::RIGHT_BUTTON_DOWN, oPos.X(), oPos.Y() );
			CMessenger::GlobalPush( oMouseMessage ); 
			CheckForCombo( CMouseMessage::RIGHT_BUTTON_DOWN, oOriginalPos );
			break;
		}
		case WM_LBUTTONUP:
		{
			u32 uX( ( static_cast< u32 >( rWindowsMessage.GetParameter2() ) << 16 ) >> 16 );
			u32 uY( static_cast< u32 >( rWindowsMessage.GetParameter2() ) >> 16 );
			CU32Vector2 oOriginalPos( CU32Vector2( uX, uY ) );
			CF32Vector2 oPos( ConvertCoordinates( oOriginalPos ) );
			CMouseMessage oMouseMessage( CMouseMessage::LEFT_BUTTON_UP, oPos.X(), oPos.Y() );
			CMessenger::GlobalPush( oMouseMessage );
			CheckForCombo( CMouseMessage::LEFT_BUTTON_UP, oOriginalPos );
			break;
		}
		case WM_RBUTTONUP:
		{
			u32 uX( ( static_cast< u32 >( rWindowsMessage.GetParameter2() ) << 16 ) >> 16 );
			u32 uY( static_cast< u32 >( rWindowsMessage.GetParameter2() ) >> 16 );
			CU32Vector2 oOriginalPos( CU32Vector2( uX, uY ) );
			CF32Vector2 oPos( ConvertCoordinates( oOriginalPos ) );
			CMouseMessage oMouseMessage( CMouseMessage::RIGHT_BUTTON_UP, oPos.X(), oPos.Y() );
			CMessenger::GlobalPush( oMouseMessage ); 
			CheckForCombo( CMouseMessage::RIGHT_BUTTON_UP, oOriginalPos );
			break;
		}
		case WM_MOUSEMOVE:
		{
			i16 iX( ( (u32)rWindowsMessage.GetParameter2() << 16 ) >> 16 );
			i16 iY( (u32)rWindowsMessage.GetParameter2() >> 16 );
			iX = NUtilities::Max< i16 >( 0, iX );
			iY = NUtilities::Max< i16 >( 0, iY );
			f32 fWindowHalfWidth = m_rWindow.GetVirtualWidth() / 2.f;
			f32 fWindowHalfHeight = m_rWindow.GetVirtualHeight() / 2.f;
			f32 fX = ( static_cast< f32 >( iX ) * m_rWindow.GetScale() ) - fWindowHalfWidth;
			f32 fY = ( static_cast< f32 >( iY ) * m_rWindow.GetScale() ) - fWindowHalfHeight;
			CMouseMessage oMouseMessage( CMouseMessage::DRAG, fX, fY );
			CMessenger::GlobalPush( oMouseMessage ); 
			break;
		}
		case WM_CAPTURECHANGED:
		{
			CMouseMessage oMouseMessage( CMouseMessage::LOSE_CONTROL, 0.f, 0.f );
			CMessenger::GlobalPush( oMouseMessage ); 
			break;
		}
		case WM_MOUSEWHEEL:
		{
			i16 wheelMovement = HIWORD( rWindowsMessage.GetParameter1() );
			CMouseMessage oMouseMessage( CMouseMessage::WHEEL, static_cast< f32 >( wheelMovement ), 0.f );
			CMessenger::GlobalPush( oMouseMessage ); 
			break;
		}
		default:
			assert( false );
	}
}

CWindowsInputListener& CWindowsInputListener::operator=( const CWindowsInputListener& )
{
	assert( false );
	return *this;
}

void CWindowsInputListener::CheckForCombo(CMouseMessage::EMouseAction eMouseAction, CU32Vector2 oMouseActionLocation)
{
	switch (m_eLastMouseAction)
	{
		case CMouseMessage::LEFT_BUTTON_DOWN:
		{
			f32 fElapsedTime( m_oLastMouseActionTimer.GetElapsedTime() );
			u32 uDistSq( m_oLastMouseActionLocation.GetDistanceSquared( oMouseActionLocation ) );
			//DEBUG_MESSAGE( "click time %f\n", fElapsedTime );
			//DEBUG_MESSAGE( "click distance %d\n", m_oLastMouseActionLocation.GetDistanceSquared( oMouseActionLocation ) );
			if (eMouseAction == CMouseMessage::LEFT_BUTTON_UP && 
				fElapsedTime <= m_fClickSpeed &&
				uDistSq <= m_uClickDistanceSquared )
			{
				CF32Vector2 oPos( ConvertCoordinates( oMouseActionLocation ) );
				CMouseMessage oMouseMessage( CMouseMessage::LEFT_BUTTON_CLICK, oPos.X(), oPos.Y() );
				CMessenger::GlobalPush( oMouseMessage ); 
			}
			break;
		}
		case CMouseMessage::RIGHT_BUTTON_DOWN:
		{
			if (eMouseAction == CMouseMessage::RIGHT_BUTTON_UP && 
				m_oLastMouseActionTimer.GetElapsedTime() <= m_fClickSpeed &&
				m_oLastMouseActionLocation.GetDistanceSquared( oMouseActionLocation ) <= m_uClickDistanceSquared )
			{
				CF32Vector2 oPos( ConvertCoordinates( oMouseActionLocation ) );
				CMouseMessage oMouseMessage( CMouseMessage::RIGHT_BUTTON_CLICK, oPos.X(), oPos.Y() );
				CMessenger::GlobalPush( oMouseMessage ); 
			}
			break;
		}
	}
	m_eLastMouseAction = eMouseAction;
	m_oLastMouseActionLocation = oMouseActionLocation;
	m_oLastMouseActionTimer.StartOrRestart();
}

CF32Vector2 CWindowsInputListener::ConvertCoordinates(CU32Vector2 oPos)
{
	f32 fWindowHalfWidth = m_rWindow.GetVirtualWidth() / 2.f;
	f32 fWindowHalfHeight = m_rWindow.GetVirtualHeight() / 2.f;
	f32 fX = ( static_cast< f32 >( oPos.X() ) * m_rWindow.GetScale() ) - fWindowHalfWidth;
	f32 fY = ( static_cast< f32 >( oPos.Y() ) * m_rWindow.GetScale() ) - fWindowHalfHeight;
	return CF32Vector2( fX, fY );
}
