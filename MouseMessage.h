#ifndef MOUSEMESSAGE_H
#define MOUSEMESSAGE_H

#include "../Messenger/Message.h"

class CMouseMessage : public CMessage
{
public:
	enum EMouseAction
	{
		NONE,
		LEFT_BUTTON_DOWN,
		MIDDLE_BUTTON_DOWN,
		RIGHT_BUTTON_DOWN,
		LEFT_BUTTON_UP,
		MIDDLE_BUTTON_UP,
		RIGHT_BUTTON_UP,
		LEFT_BUTTON_CLICK,
		RIGHT_BUTTON_CLICK,
		LEFT_BUTTON_DOUBLE_CLICK,
		RIGHT_BUTTON_DOUBLE_CLICK,
		DRAG,
		LOSE_CONTROL,
		WHEEL
	};

	static u32	GetMouseMessageUID();

private:
	static const u32	ms_kuMouseMessageUID = 44953;

public:
					CMouseMessage( EMouseAction eMouseAction, f32 fX, f32 fY );
	EMouseAction	GetMouseAction() const;
	f32				X() const { return m_fX; }
	f32				Y() const { return m_fY; }

private:
	EMouseAction	m_eMouseAction;
	f32				m_fX;
	f32				m_fY;
};

#endif
