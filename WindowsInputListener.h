#ifndef WINDOWSINPUTLISTENER_H
#define WINDOWSINPUTLISTENER_H

#include <utility>
#include "Input/InputListener.h"
#include "Windows Timer/WindowsTimer.h"
#include "MouseMessage.h"
#include "Vector/Vector2.h"

class CWindow;

class CWindowsInputListener : public CInputListener
{
private:
	static constexpr f32	ms_kfDefaultClickSpeed = 1.f;
	static constexpr u32	ms_kuDefaultClickDistance = 5;

public:
					CWindowsInputListener() = delete;
					CWindowsInputListener( CInput* pInput, CWindow& rWindow, f32 fClickSpeed = ms_kfDefaultClickSpeed, u32 uClickDistance = ms_kuDefaultClickDistance );
	virtual			~CWindowsInputListener();
	virtual void	Push( CMessage& rMessage );

private:
	CWindowsInputListener&	operator=( const CWindowsInputListener& );
	void					CheckForCombo( CMouseMessage::EMouseAction eMouseAction, CU32Vector2 oMouseActionLocation );
	CF32Vector2				ConvertCoordinates( CU32Vector2 oPos );

	static const u32	ms_kuLeftMouseDownMessageUID = 33928;

	CWindow&					m_rWindow;
	CMouseMessage::EMouseAction	m_eLastMouseAction;
	CU32Vector2					m_oLastMouseActionLocation;
	CWindowsTimer				m_oLastMouseActionTimer;
	f32							m_fClickSpeed;
	u32							m_uClickDistanceSquared;
};

#endif
