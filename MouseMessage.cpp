#include "MouseMessage.h"

u32 CMouseMessage::GetMouseMessageUID()
{
	return ms_kuMouseMessageUID;
}

CMouseMessage::CMouseMessage( EMouseAction eMouseAction, f32 fX, f32 fY )
	: CMessage( ms_kuMouseMessageUID )
	, m_eMouseAction( eMouseAction )
	, m_fX( fX )
	, m_fY( fY )
{
}

CMouseMessage::EMouseAction CMouseMessage::GetMouseAction() const
{
	return m_eMouseAction;
}
